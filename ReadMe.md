## requirements

`zsh` and [`oh-my-zsh`](https://github.com/robbyrussell/oh-my-zsh) installed

## installation

[`dircolors.256dark`](https://github.com/seebi/dircolors-solarized) should be moved to `~/.oh-my-zsh/custom`

`dkw.zsh-theme` should be moved to `~/.oh-my-zsh/custom/themes`
 
The [Hasklig](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/Hasklig/Regular/complete) font should be installed to the system. This is a [NerdFont](https://github.com/ryanoasis/nerd-fonts) augmented font that has all of the powerline characters and a lot of other glyphs.

`.zshrc` goes ... you know

## optional

[`solarized-mod`](https://github.com/oumu/mintty-color-schemes) is included for terminal colors, it could be installed to the location your terminal gets color list from, e.g. 

`C:/cygwin64/usr/share/mintty/themes/` (for CygWin MinTTY location) or 

`C:/Users/user/AppData/Local/wsltty/usr/share/mintty/themes/` (for [wsl-terminal](https://github.com/goreliu/wsl-terminal))

Vim instructions assume you are using NeoBundle or similar:

```vim
NeoBundle 'altercation/vim-colors-solarized'
let g:solarized_termtrans=1
set t_Co=256
set background=dark
colorscheme solarized
```
