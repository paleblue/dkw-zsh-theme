# uncomment if you want the right prompt to have no gutter -- however this can cause problems with cursor placement
# ZLE_RPROMPT_INDENT=0

local -a bgclrs
local -a txts
local -a fgclrs
local -A excps
local -a bgrclrs
local -a rtxts
local -a fgrclrs

# visible versions
local -a _bgclrs
local -a _fgclrs
local -a _txts


local SHOW_ARROW=1
local PREV_BG
local CUR_BG
local CUR_FG
local LPRMPT

function set_foreground_color() {
    echo "%{\x1b[38;2;$1m%}"
}

function set_background_color() {
    echo "%{\x1b[48;2;$1m%}"
}

function reset_background_color() {
	echo "%{\x1b[0m%}"
}

function append_segment() {
    # $1 content
    # $2 fg
    # $3 bg

    local outp=""
    local cntnt=$1
    local CUR_FG=$2
    local CUR_BG=$3
    local isFirst=0
    if [[ -z $LPRMPT ]]; then
        isFirst=1
    fi
    # only output if it's not empty
    if [[ ! -z ${cntnt// } ]]; then
        if [[ $isFirst = 1 ]]; then
            # new prompt, initialize fg
            outp+=$(set_foreground_color $CUR_FG)
            PREV_BG=
        fi
        outp+=$(set_background_color $CUR_BG)
        if [[ $SHOW_ARROW = 1 ]]; then
          if [[ $# -lt 3 ]]; then
            # arrow on undefined BG needs a reset background
            # but then the foreground needs to be set again
            outp+=$(reset_background_color)
            outp+=$(set_foreground_color $PREV_BG)
          fi
          # draw arrow
          outp+=""
        fi
        outp+=$(set_foreground_color $CUR_FG)
        outp+=$cntnt

        # set for next element's arrow
        outp+=$(set_foreground_color $CUR_BG)
        PREV_BG=$CUR_BG
    fi
    LPRMPT+=$outp
}

function rappend_segment() {
    # $1 content
    # $2 fg
    # $3 bg

    local outp=""
    local cntnt=$1
    local CUR_FG=$2
    local CUR_BG=$3
    # only output if it's not empty
    if [[ ! -z ${cntnt// } ]]; then
        if [[ $SHOW_ARROW = 1 ]]; then
          # draw arrow
          outp+=$(set_foreground_color $CUR_BG)
          outp+=""
        fi
        outp+=$(set_background_color $CUR_BG)
        outp+=$(set_foreground_color $CUR_FG)
        outp+=$cntnt
    fi
    RPRMPT+=$outp
}

function __appendroot() {
    append_segment "  ArchVM " $THEME_LIGHTEST $THEME_DARKEST
}

function __appendconda() {
    if [[ ! -z $CONDA_DEFAULT_ENV ]]; then
        append_segment " $CONDA_DEFAULT_ENV " $THEME_LIGHT2 $THEME_DARK2
    fi
}

function __appendstatus() {
    local mybg=$THEME_DARK3
    local myfg=$THEME_RED
    local nextbg=$THEME_LIGHT2
    # if I show, then set bg, arrow, set fg, text, set fg=bg, arrow
    # if I don't show, then set bg to next, arrow
    COND_FALSE=$(set_background_color $nextbg)
    COND_FALSE+=""

    COND_TRUE=$(set_background_color $mybg)
    COND_TRUE+=""
    COND_TRUE+=$(set_foreground_color $myfg)
    COND_TRUE+=" %? "
    COND_TRUE+=$(set_foreground_color $mybg)
    COND_TRUE+=$(set_background_color $nextbg)
    COND_TRUE+=""

    LPRMPT+="%(?.$COND_FALSE.$COND_TRUE)"
}

function __appendpwd() {
    local content=" "
    if [[ ${(%):-%c} = "~" ]]; then
	  content+=""
    else
	  content+=""
    fi
    content+=' %c '
    append_segment $content $THEME_DARK2 $THEME_LIGHT2
}

function __appendgit() {
    local content
    content=" $(  git rev-parse --abbrev-ref HEAD 2> /dev/null || echo '' ) "
    append_segment $content $THEME_YELLOW $THEME_DARK2
}

function __appendtail() {
    append_segment " » " $THEME_RED
}

function __rappendpath() {
    rappend_segment " %~ " $THEME_DARK2 $THEME_MID2
}

function __rappendtime() {
    rappend_segment "  %T " $THEME_DARK2 $THEME_LIGHT2
}

setopt prompt_subst
THEME_DARKEST='0;33;54'
THEME_DARK2='17;38;66'
THEME_DARK3='22;48;82'
THEME_MID1='83;89;115'
THEME_MID2='101;108;131'
THEME_LIGHT2='140;132;156'
THEME_LIGHTEST='240;224;215'
THEME_YELLOW='181;115;0'
THEME_RED='220;50;47'

function _updatePRMPT() {
    LPRMPT=""
    SHOW_ARROW=0; __appendroot; SHOW_ARROW=1
    __appendconda
    __appendstatus
    SHOW_ARROW=0; __appendpwd; SHOW_ARROW=1 # arrow controlled by status
    __appendgit
    __appendtail
    LPRMPT+="%{$reset_color%}"

    RPRMPT=""
    __rappendpath
    __rappendtime
    RPRMPT+="%{$reset_color%}"
}

typeset -a precmd_functions
precmd_functions+=(_updatePRMPT)

function _prompt() {
    echo "\$LPRMPT"
}

function _rprompt() {
    echo "\$RPRMPT"
}

PROMPT=$(_prompt)
RPS1=$(_rprompt)
