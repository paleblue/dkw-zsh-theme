# uncomment if you want the right prompt to have no gutter -- however this can cause problems with cursor placement
# ZLE_RPROMPT_INDENT=0

local -a bgclrs
local -a txts
local -a fgclrs
local -A excps
local -a bgrclrs
local -a rtxts
local -a fgrclrs

RESETBG_FLAG='xabcx'

function _write_color_text() {
    local outp=""

    # BG, or reset if an empty string
    if [ "$#" -gt 2 ]; then
	if [ $2 = $RESETBG_FLAG ] || [ $3 = $RESETBG_FLAG ]; then
	    outp="$outp%{\x1b[0m%}"
	else
	    outp="$outp%{\x1b[48;2;$3m%}"
	fi
    fi

    # FG
    if [ "$#" -gt 1 ]; then
	if [ $2 = $RESETBG_FLAG ]; then
	    outp="$outp%{\x1b[0m%}"
	else
	    outp="$outp%{\x1b[38;2;$2m%}"
	fi
    fi

    outp="$outp$1"
    echo $outp
}

function _build_prompt() {
    local outp=""
    for pitem in {1..${#fgclrs}}; do
	# don't draw start arrow if it's the first item, or if the previous item was an exception
	if ((pitem > 1 && !${+excps["e$(($pitem))"]} && !${+excps["e$(($pitem-1))"]} )); then
	    outp=$outp$(_write_color_text "" $bgclrs[$(($pitem-1))] $bgclrs[$pitem])
	fi

	if (( ${+excps["e$pitem"]} )); then
	    # if an exception exists for this item, draw it directly
	    outp=$outp$excps["e$pitem"]
	elif [ ! -z $txts[$pitem] ]; then
	    # otherwise, if the text isn't empty, draw normal vars
	    outp=$outp$(_write_color_text $txts[$pitem] $fgclrs[$pitem] $bgclrs[$pitem])
	fi

	# only draw end arrow if it's the last item (otherwise the arrow is the responsibility of the next item)
	if [ $pitem -eq ${#fgclrs} ]; then
	    if [ ! $bgclrs[$(($pitem))] = $RESETBG_FLAG ]; then
		outp=$outp%{$reset_color%}$(_write_color_text "" $bgclrs[$(($pitem))])
	    fi
	fi
    done
    outp="$outp%{$reset_color%}"
    echo $outp
}

function _build_rprompt() {
    local outp=""
    for pitem in {1..${#fgrclrs}}; do
	if ((pitem == 1)); then
	    outp=$outp"%{$reset_colors%}"$(_write_color_text "" $bgrclrs[$pitem])
	else
	    outp=$outp$(_write_color_text "" $bgrclrs[$pitem] $bgrclrs[$pitem-1])
	fi

	if [ ! -z $txts[$pitem] ]; then
	    # otherwise, if the text isn't empty, draw normal vars
	    outp=$outp$(_write_color_text $rtxts[$pitem] $fgrclrs[$pitem] $bgrclrs[$pitem])
	fi
    done
    outp="$outp%{$reset_color%}"
    echo $outp
}

function _add_exception() {
    # $1 position
    # $2 test
    # $3 false
    # $4 true
    COND_NUM=$1
    COND_TEST="$2"
    COND_FALSE=$(_write_color_text "$3" $bgclrs[$((COND_NUM - 1))] $bgclrs[$((COND_NUM + 1))])
    COND_TRUE=$(_write_color_text "" $bgclrs[$((COND_NUM - 1))] $bgclrs[$((COND_NUM))])\
$(_write_color_text $4 $fgclrs[$((COND_NUM))] $bgclrs[$((COND_NUM))])\
$(_write_color_text "" $bgclrs[$((COND_NUM))] $bgclrs[$((COND_NUM + 1))])

    excps["e$COND_NUM"]="%($COND_TEST.$COND_FALSE.$COND_TRUE)"
}

function _decoratePWD() {
    if [[ ${(%):-%c} = "~" ]]; then
	HOMEDEC=""
    else
	HOMEDEC=""
    fi
}

setopt PROMPT_SUBST
SOLARIZED_BASE03='0;43;54'
SOLARIZED_BASE02='7;54;66'
SOLARIZED_BASE01='88;110;117'
SOLARIZED_BASE00='101;123;131'
SOLARIZED_BASE0='131;148;150'
SOLARIZED_BASE1='147;161;161'
SOLARIZED_BASE2='238;232;213'
SOLARIZED_BASE3='253;246;227'
SOLARIZED_YELLOW='181;137;0'
SOLARIZED_ORANGE='203;75;22'
SOLARIZED_RED='220;50;47'
SOLARIZED_MAGENTA='211;54;130'
SOLARIZED_VIOLET='108;113;196'
SOLARIZED_BLUE='38;139;210'
SOLARIZED_CYAN='42;161;152'
SOLARIZED_GREEN='133;153;0'

HOMEDEC=""

fgclrs+=$SOLARIZED_BASE3
fgclrs+=$SOLARIZED_RED
fgclrs+=$SOLARIZED_BASE02
fgclrs+=$SOLARIZED_YELLOW
fgclrs+=$SOLARIZED_RED

txts+="  "
txts+=" x " # only necessary to keep arrays synched
txts+=" \$HOMEDEC %c "
txts+=' $(  git rev-parse --abbrev-ref HEAD 2> /dev/null || echo "" ) '
txts+=" » "

bgclrs+=$SOLARIZED_BASE03
bgclrs+=$SOLARIZED_BASE02
bgclrs+=$SOLARIZED_BASE1
bgclrs+=$SOLARIZED_BASE02
bgclrs+=$RESETBG_FLAG

_add_exception 2 "?" "" " %? "

fgrclrs+=$SOLARIZED_BASE02
fgrclrs+=$SOLARIZED_BASE02

rtxts+=" %~ "
rtxts+="  %T "

bgrclrs+=$SOLARIZED_BASE00
bgrclrs+=$SOLARIZED_BASE1

typeset -a precmd_functions
precmd_functions+=(_decoratePWD)

PROMPT=$(_build_prompt)
RPS1=$(_build_rprompt)
