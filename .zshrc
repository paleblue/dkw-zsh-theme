export ZSH=$HOME/.oh-my-zsh

# cygwin or wsl?
if [[ $HOME = *"cygdrive"* ]]; then
    export MOUNTBASE=/cygdrive
else
    export MOUNTBASE=/mnt
    export PATH=~/.local/bin:$PATH
fi

# X config
export DISPLAY=localhost:0.0
export LANG=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8

eval `dircolors $HOME/.oh-my-zsh/custom/dircolors.256dark`
zstyle ':completion:*' list-colors "${(@s.:.)LS_COLORS}"
autoload -Uz compinit
compinit -C

alias lah='ls -lah'
alias duh='du -h -d 1'

alias ggrep='grep -RniIs --exclude-dir node_modules --exclude-dir test --exclude-dir .git'
alias wgit="$MOUNTBASE/c/Program\ Files/Git/bin/git.exe"

alias clr="cd ~ && clear"

alias clip="$MOUNTBASE/c/Windows/System32/clip.exe"

export WORKON_HOME=$HOME/.virtualenv
export PROJECT_HOME=/mnt/e/Projects/labs/py
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
export VIRTUALENV_PYTHON=/usr/bin/python3
source ~/.local/bin/virtualenvwrapper.sh

export PATH=/usr/local/bin:$PATH
unsetopt beep

ZSH_THEME="dkw2"
plugins=(
    git
)

source $ZSH/oh-my-zsh.sh

